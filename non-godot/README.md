# non-godot files
*These are the non-godot files for the game project.*
# Folders
- `gimp` : `.xcf` files for visual work. This is where the pixel art is made,
  but also where graphical stuff like map layouts because no paint on linux.
- `writing` : `.md` and `.txt` files containing notes about the project.
