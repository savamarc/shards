extends VBoxContainer

func _ready():
  if $NewButton/Label.connect("pressed", self, "launch_game"):
    print("Error")
    return
  if $OptionsButton/Label.connect("pressed", self, "show_options"):
    print("Error")
    return
  if $ExitButton/Label.connect("pressed", self, "exit_game"):
    print("Error")
    return

func launch_game():
  if get_tree().change_scene("res://Main.tscn"):
    print("Error")
    return

func show_options():
  if get_tree().change_scene("res://UI/OptionMenu.tscn"):
    print("Error")
    return

func exit_game():
  get_tree().quit()