#include <gdnative_api_struct.gen.h>

/*
 * This si the 64 bits implementation of a custom 64 bits mrg. This is probably
 * statistically bad, but is unbiased and allows me to implement other unbiased
 * rng usage.
 *
 * This is done mostly because it is easier for me to do this than to go check
 * that godot does stuff as I would like.
 * */

#include <stdint.h>

#define m1 137438933327
#define m2 137438924159
#define a12 18997718
#define a13 38584692
#define a21 412406
#define a23 31336619
#define norm 7.27595868065100276763982074353101438431157266251148030278272926807403564453125e-12
#define SEED 1234

uint64_t mrg64_x10 = SEED, mrg64_x11 = SEED, mrg64_x12 = SEED,
         mrg64_x20 = SEED, mrg64_x21 = SEED, mrg64_x22 = SEED;

const godot_gdnative_core_api_struct *api = NULL;
const godot_gdnative_ext_nativescript_api_struct *nativescript_api = NULL;

void *mrg64_constructor(godot_object *p_instance, void *p_method_data);
void mrg64_destructor(godot_object *p_instance, void *p_method_data, void *p_user_data);

godot_variant mrg64_get_double(godot_object *p_instance, void *p_method_data,
    void *p_user_data, int p_num_args, godot_variant **p_args);

godot_variant mrg64_get_int(godot_object *p_instance, void *p_method_data,
    void *p_user_data, int p_num_args, godot_variant **p_args);

void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *p_options) {
  api = p_options->api_struct;

  // Now find our extensions.
  for (int i = 0; i < api->num_extensions; i++) {
    switch (api->extensions[i]->type) {
      case GDNATIVE_EXT_NATIVESCRIPT: {
                                        nativescript_api = (godot_gdnative_ext_nativescript_api_struct *)api->extensions[i];
                                      }; break;
      default: break;
    }
  }
}

void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *p_options) {
  api = NULL;
  nativescript_api = NULL;
}

void GDN_EXPORT godot_nativescript_init(void *p_handle) {
  godot_instance_create_func create = { NULL, NULL, NULL };
  create.create_func = &mrg64_constructor;

  godot_instance_destroy_func destroy = { NULL, NULL, NULL };
  destroy.destroy_func = &mrg64_destructor;

  nativescript_api->godot_nativescript_register_class(p_handle, "mrg64", "Reference",
      create, destroy);

  godot_instance_method get_double = { NULL, NULL, NULL };
  get_double.method = &mrg64_get_double;

  godot_method_attributes attributes = { GODOT_METHOD_RPC_MODE_DISABLED };

  nativescript_api->godot_nativescript_register_method(p_handle, "mrg64", "get_double",
      attributes, get_double);
}

void *mrg64_constructor(godot_object *p_instance, void *p_method_data) {
  return NULL;
}

void mrg64_destructor(godot_object *p_instance, void *p_method_data, void *p_user_data) {
  return;
}

godot_variant mrg64_get_double(godot_object *p_instance, void *p_method_data,
    void *p_user_data, int p_num_args, godot_variant **p_args) {
  godot_variant ret;
  uint64_t p, r;

  /* Combination */
  r = (mrg64_x12 + mrg64_x22)%m1;
  r %= m1;

  /* Component 1 */
  p = (a12 * mrg64_x11 + a13 * mrg64_x10) % m1;
  mrg64_x10 = mrg64_x11;
  mrg64_x11 = mrg64_x12;
  mrg64_x12 = p;

  /* Component 2 */
  p = (a21 * mrg64_x22 + a23 * mrg64_x20) % m2;
  mrg64_x20 = mrg64_x21;
  mrg64_x21 = mrg64_x22;
  mrg64_x22 = p;

  api->godot_variant_new_real(&ret, r*norm);

  return ret;
}
