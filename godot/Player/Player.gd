"""
Manages Player physics and input, as well as the camera.
Stores most information of the game.
"""
extends KinematicBody2D

# Movement
var _speed = 175
var _velocity = Vector2(0,0)
var _floor_normal = Vector2(0,-1)
var _direction = 1
# If _accel is true, the next _accel_frames will not process physics as usual.
# This can be set with apply_accel
# This is usefull for damage bumps and dashes
var _accel = false
var _accel_frames = 0
var _accel_value = Vector2(0,0)

# Crouching
var _crouched = false

#Jumping
var _jump_height = -450
var _fall_speed = 600
#var default_snap = Vector2(0, 10)
#var snap = default_snap
var _gravity = 40
var _jumping_frames = 10
var _jumped_frames = 0
var _on_floor

# Damage taking
var iframes = 10
var boop_frames = 7
var d_boost = Vector2(-300, 0)

# Upgrades
var _upgrades = 0
# Upgrade 0: Dash
var _dash = null
# var _dash_upgrade = false
# var _dash_on = false
# var _dash_speed = Vector2(500, -50)
# var _dash_frames = 15
# var _dash_cooldown = 0
# var _dash_cooldown_frames = 40

# Camera
var _camera_frames = 0
var _camera_destination = [0, 0, 0, 0]

signal damage_taken(ammount)

func _ready():
  $Camera.make_current()
  _update_internals()

func get_movement():
  # Accelerated state.
  # Shortcuts physics
  if _accel:
    _accel_frames -= 1
    if _accel_frames<=0:
      _accel = false
    _velocity += _accel_value
    $Node2D.velocity = _velocity
    return _velocity

  # Dash action
  # Applies accelerated state and shortcuts physics
  if _dash and Input.is_action_just_pressed("move_dash"):
    if !_dash.activate_ability():
      $Node2D.velocity = _velocity
      return _velocity

  # x velocity
  if Input.is_action_pressed("move_left") and Input.is_action_pressed("move_right"):
    _velocity.x = 0
  elif Input.is_action_pressed("move_right"):
    $PhyHit/RightWall.force_raycast_update()
    if not $PhyHit/RightWall.is_colliding():
      _velocity.x = _speed
      _direction = 1
      $Sprite.flip_h = false
  elif Input.is_action_pressed("move_left"):
    $PhyHit/LeftWall.force_raycast_update()
    if not $PhyHit/LeftWall.is_colliding():
      _velocity.x = -_speed
      _direction = -1
      $Sprite.flip_h = true
  else:
    _velocity.x = 0

  if _crouched:
    if Input.is_action_pressed("move_up") || abs(_velocity.x) > 0:
      _crouched = false
      position.y -= $PhyHit.shape.extents.y/2
      $PhyHit.scale.y = 1
      $Sprite.scale.y = 1
  if Input.is_action_pressed("move_crouch"):
    if !_crouched:
      position.y += $PhyHit.shape.extents.y/2
      $PhyHit.scale.y = 0.5
      $Sprite.scale.y = 0.5
      _velocity.x = 0
    _crouched = true

  # y velocity
  if _velocity.y < _fall_speed:
    _velocity.y += _gravity
  if _on_floor:
    if Input.is_action_just_pressed("move_jump"):
      _velocity.y = _jump_height
      _jumped_frames = _jumping_frames
      position.y-=2
  else:
    if _jumped_frames:
      if Input.is_action_pressed("move_jump"):
        _jumped_frames -= 1
        _velocity.y = _jump_height
      elif Input.is_action_just_released("move_jump"):
        _jumped_frames = 0
      if is_on_ceiling():
        _jumped_frames = 0

  $Node2D.velocity = _velocity
  return _velocity

func _physics_process(delta):
  # The high number of slide count is for movement on slopes
  $PhyHit/FloorCheck.force_raycast_update()
  _on_floor = $PhyHit/FloorCheck.is_colliding()
  var pos = position
  _velocity = move_and_slide(get_movement(), _floor_normal)
  if (pos-position).length() < 1:
    position = pos

func _process(delta):
  if _camera_frames:
    $Camera.limit_left += (_camera_destination[0]-$Camera.limit_left)/_camera_frames
    $Camera.limit_right += (_camera_destination[1]-$Camera.limit_right)/_camera_frames
    $Camera.limit_top += (_camera_destination[2]-$Camera.limit_top)/_camera_frames
    $Camera.limit_bottom += (_camera_destination[3]-$Camera.limit_bottom)/_camera_frames
    _camera_frames -= 1

  if abs(_velocity.x)>0:
    $Sprite.play()
  else:
    $Sprite.stop()
    $Sprite.set_frame(0)

  if Input.is_action_just_pressed("shoot"):
    $Gun._shoot(_direction)

func _uncrouch():
  pass

func _change_camera(new_limits):
  # Sets new limits for the camera. The camera will reposition in _camera_frames frames.
  _camera_destination = new_limits
  _camera_frames = 10

func _update_internals():
  # Update internal values for power ups.
  # Called when obtaining a new powerup and when loading the character
  var up_left = _upgrades
  var num = 0
  while up_left > 0:
    if up_left&1:
      if num == 0:
        if !_dash:
          var dash_inst = load("res://Player/Upgrades/Dash.gd")
          _dash = dash_inst.new()
          add_child(_dash)
      elif num == 1:
        pass
    up_left = up_left>>1
    num += 1
  pass

func set_power(number):
  # If upgrade 'number' is not collected, adds it to the character
  if !((_upgrades>>number) & 1):
    _upgrades += 1<<number
    _update_internals()

func apply_accel(set_speed, accel_vect, accel_frames):
  """
  Applies 'set_speed' velocity to the player (ajusts x speed depending on
  direction) and makes it so that the player is accelerated by 'accel_vect' for
  the next 'accel_frames' frames.
  """
  if accel_frames <= 0:
    return
  _velocity = Vector2(_direction*set_speed.x, set_speed.y)
  _jumped_frames = 0
  _accel_frames = accel_frames
  _accel = true
  _accel_value = accel_vect

func take_dmg(amount):
  emit_signal("damage_taken", amount)
  apply_accel(d_boost, Vector2(0,0), boop_frames)
