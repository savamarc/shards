"""
This is a node to store various game properties.
Everything is here so that it is easier to interact with and repopulate from a
stopped state.
"""
extends Node

# Player related
var player_life = 100
var player_life_total = 100

# Save state related
var map_area_dict = ['res://Levels/Landing/Map.tscn',
                     'res://Levels/Plains/Map01.tscn',
                     'res://Levels/Plains/Map02.tscn',
                     'res://Levels/Plains/Map03.tscn',
                     'res://Levels/Plains/Map04.tscn',]
var map_area

# This is a signal that will be emited when this object changes and the UI needs to be rewritten
signal life_changed(new_percent)

signal player_dies

func _ready():
  pass

func init(state):
  if state == 0:
    map_area = 0
  emit_signal("life_changed", int(100*(float(player_life/player_life_total))))

func take_dmg(ammount):
  player_life -= ammount
  if player_life <= 0:
    emit_signal("player_dies")
  emit_signal("life_changed", int(100*(float(player_life)/player_life_total)))

