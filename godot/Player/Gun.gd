extends Node

const bullet = preload("res://Player/Bullet.tscn")

func _ready():
  # Called when the node is added to the scene for the first time.
  # Initialization here
  pass

func _shoot(direction):
  var shot = bullet.instance()
  get_tree().get_root().add_child(shot)
  shot.position = get_parent().position-Vector2(0,3)
  shot.set_direction(direction)
  return shot
