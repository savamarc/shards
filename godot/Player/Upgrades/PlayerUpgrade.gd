extends Node
"""
This is a class to act as an interface to upgrades.
It already implements cooldown management.

To add:
  - Sound/Visual
  - Interface passive buffs/debuffs
"""

var cooldown = 0
var cooldown_length = 0
var active = true
var active_frames
var activation_conditions = []

func _process(delta):
  if cooldown>0:
    cooldown -= 1
  _check_activation()

func activate_ability():
  return 0

func _check_activation():
  pass
