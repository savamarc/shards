extends Area2D
"""
This class represents an upgrade.
Upgrades simply need to be instanciated on the map with their number, a sprite and a message.
They are not to be implemented in separate files.

Each upgrade possesses a number. This is because the player class stores its
upgrades in an integer where the bit 'i' is worth '1' if the player has collected the upgrade 'i'.

As of now the upgrades are
0: Dash
1: ?
"""

export (String) var message    # This is printed on screen when collecting the upgrade
export (int)    var up_number  # The number representing this upgrade

func _ready():
  if connect("body_entered", self, "_power_up"):
    print("Error")

func _power_up(body):
  # Gives its power to the player
  # Loads a TextBox with 'message' in the game tree
  body.set_power(up_number)
  var textbox = get_tree().root.get_node("Main/GUI/InGameUI/TextBox")
  textbox.set_text(message)
  textbox.show()
  get_tree().paused = true
  queue_free()