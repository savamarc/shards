extends "PlayerUpgrade.gd"

var _dash_speed = Vector2(500, -50)
var _dash_slow = Vector2(0,5)
var _dash_frames = 15

func _init():
  cooldown_length = 50

func activate_ability():
  if cooldown or !active:
    return 1
  cooldown = cooldown_length
  active = false
  get_parent().apply_accel(_dash_speed, _dash_slow, _dash_frames)
  return 0

func _check_activation():
  # Dash cooldown. Replenishes only when touching the floor
  if !active && cooldown == 0 && get_parent()._on_floor:
    active = true
