extends Area2D

var dmg_value = 1
var speed = 6
var direction
signal collision(body)
var num_frames = 4
var ellapsed_frames = 0

func _ready():
  if $Lifetime.connect("timeout", self, "_unload"):
    print("Error")
    return
  if connect("collision", self, "hit_something"):
    print("Error")
    return

func _physics_process(delta):
  var results = get_world_2d().direct_space_state.intersect_ray(position, Vector2(position.x + speed*direction, position.y), [self], collision_mask)
  if results:
    position.x = results['position'].x
    emit_signal("collision", results['collider'])
  else:
    position.x += speed*direction
  ellapsed_frames += 1
  if ellapsed_frames == num_frames:
    ellapsed_frames = 0
    $Sprite.set_flip_v(!$Sprite.flip_v)

#func _process(delta):
#  pass

func _unload():
  queue_free()

func hit_something(body):
  body.take_dmg(dmg_value)
  _unload()

func set_direction(side):
  self.direction = side
  self.scale.x *= side
