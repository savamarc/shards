extends Area2D

var active = false
signal request_save
var entity

func _ready():
  if connect('body_entered', self, '_activate'):
    print("Error")
    return
  if connect('body_exited', self, '_deactivate'):
    print("Error")
    return

func _process(delta):
  if active:
    if Input.is_action_pressed('move_up'):
      emit_signal('request_save')

func _activate(body):
  entity = body
  active = true

func _deactivate(body):
  active = false

func save():
  var dict = {'spawn_x':self.position.x,
    'spawn_y':self.position.y}
  print(dict)
  return dict
