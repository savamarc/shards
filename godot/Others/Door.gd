extends StaticBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
  if $Collision.connect("body_entered", self, "open"):
    print("Error")
    return
  if $Collision.connect("body_exited", self, "close"):
    print("Error")
    return

func open(body):
  self.hide()
  self.set_collision_layer_bit(1, 0)

func close(body):
  self.show()
  self.set_collision_layer_bit(1,1)

func take_dmg(amount):
  pass