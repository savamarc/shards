extends Area2D

export (int) var WarpArea
export (int) var WarpNo

var active = false

signal change_map_request(next_zone, spawn_no)

func _ready():
  if connect("body_entered", self, "_activate"):
    print("Error")
    return
  if connect("body_exited", self, "_deactivate"):
    print("Error")
    return

func _activate(body):
  active = true

func _deactivate(body):
  active = false

func _process(delta):
  if active:
    if Input.is_action_just_pressed("move_up"):
      emit_signal("change_map_request", WarpArea, WarpNo)
