extends StaticBody2D

var dmg_value = 10

func _ready():
  if $SpikesArea.connect("body_entered", self, "_body_entered"):
    print("Error")
    return

func _body_entered(body):
  body.take_dmg(dmg_value)

func take_dmg(amount):
  pass