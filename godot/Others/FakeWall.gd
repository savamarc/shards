extends Area2D

func _ready():
  if connect("body_entered", self, "_hide"):
    print("Error")
    return
  if connect("body_exited", self, "_show"):
    print("Error")
    return

func _hide(body):
  self.hide()

func _show(body):
  self.show()

func take_dmg(amount):
  pass