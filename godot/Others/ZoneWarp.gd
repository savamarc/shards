extends Area2D

export (int) var WarpArea
export (int) var WarpNo

signal change_map_request(next_zone, spawn_no)

func _ready():
  if connect("body_entered", self, "_activate"):
    print("Error")
    return

func _activate(body):
  call_deferred('emit_signal', "change_map_request", WarpArea, WarpNo)
