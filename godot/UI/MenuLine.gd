extends HBoxContainer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
  if $Label.connect("mouse_entered", self, "_on_Container_mouse_entered"):
    print("Error")
    return
  if $Label.connect("mouse_exited", self, "_on_Container_mouse_exited"):
    print("Error")
    return
  $Cont/Square.hide()

#func _process(delta):
#  # Called every frame. Delta is time since last frame.
#  # Update game logic here.
#  pass


func _on_Container_mouse_entered():
  $Cont/Square.show()


func _on_Container_mouse_exited():
  $Cont/Square.hide()
