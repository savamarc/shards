extends CenterContainer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
  if $Aligner/Button.connect("pressed", self, "go_back"):
    print("Error")
    return

func go_back():
  if get_tree().change_scene("res://MainMenu.tscn"):
    print("Error")
    return
