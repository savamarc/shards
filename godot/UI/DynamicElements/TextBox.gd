extends NinePatchRect

var active = true

func _ready():
  if connect("visibility_changed", self, "_activate"):
    print("Error")
  
func _process(delta):
  if active and get_tree().paused and Input.is_action_pressed("ui_accept"):
    hide()
    get_tree().paused = false

func _activate():
  if visible:
    active = true
  else:
    active = false
    
func set_text(text):
  $Text.text = text
  $Text.set_anchors_and_margins_preset(Control.PRESET_CENTER)