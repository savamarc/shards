extends Panel
var frames = 0

func _ready():
  $Label.text = 'Loading'

func _process(delta):
  if visible:
    frames += 1
    if frames == 15:
      frames = 0
      if $Label.text == 'Loading...':
        $Label.text = 'Loading'
      elif $Label.text == 'Loading':
        $Label.text = 'Loading.'
      elif $Label.text == 'Loading.':
        $Label.text = 'Loading..'
      elif $Label.text == 'Loading..':
        $Label.text = 'Loading...'