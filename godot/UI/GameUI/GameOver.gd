extends CenterContainer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
  if $Things/Buttons/Replay.connect("pressed", self, "_reload"):
    print("Error")
    return
  if $Things/Buttons/Menu.connect("pressed", self, "_load_menu"):
    print("Error")
    return

func _reload():
  get_tree().paused = false
  if get_tree().change_scene("res://Main.tscn"):
    print("Error")
    return

func _load_menu():
  get_tree().paused = false
  if get_tree().change_scene("res://MainMenu.tscn"):
    print("Error")
    return