extends CenterContainer
"""
A pause menu. This processes and pauses the game by itself whenever it is in the game tree.
"""

func _ready():
  if $Resume.connect("pressed", self, "_unpause"):
    print("Error")
    return

func _process(delta):
  if Input.is_action_just_pressed("ui_pause"):
    if visible:
      _unpause()
    else:
      _pause()

func _pause():
  show()
  get_tree().paused = true

func _unpause():
  hide()
  get_tree().paused = false