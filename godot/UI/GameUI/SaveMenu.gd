extends CenterContainer

signal save_game

func _ready():
  # Sets its buttons to have the same size
  if $Things/SaveButtons/No.connect('pressed', self, 'infirm_save'):
    print("Error")
    return
  if $Things/SaveButtons/Yes.connect('pressed', self, 'confirm_save'):
    print("Error")
    return

func confirm_save():
  # For now the saving function only save the state of the player
  # This function is not safe for now. This clears the save file before writing to it
  # To be safe, this should create a temporary file and then remove the old file
  emit_signal("save_game")
  hide()
  get_tree().paused = false

func infirm_save():
  hide()
  get_tree().paused = false
