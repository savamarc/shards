"""
This is mostly a container class. This contains all the GUI elements and
controls their visibility. This also dispatches modifications to counters on the
screen such as life.

It is also possible to artificially pause the game by calling $GUI.timeout(x).
This pauses the game for x frames. This can be used to create artificial loading
times to avoid stuff going to fast.
"""
extends Node

var pause_frames = 0

func _ready():
  $SaveMenu.hide()
  $GameOver.hide()
  $PauseMenu.hide()

func _process(delta):
  if pause_frames:
    pause_frames -= 1
    if !pause_frames:
      get_tree().paused = false
      $LoadScreen.hide()

func timeout(frames):
  pause_frames = frames
  get_tree().paused = true

func update(life_percent):
  $InGameUI/HPBar.value = life_percent
  pass

func game_over():
  get_tree().paused = true
  $GameOver.show()
  $InGameUI.hide()

func save_prompt():
  get_tree().paused = true
  $SaveMenu.show()

func show_load():
  $LoadScreen.show()
