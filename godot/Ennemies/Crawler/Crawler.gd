"""
A small ennemy that moves back and forth.

Add features where it does not jump of platforms
"""
extends "res://Ennemies/Ennemy.gd"

var prev_pos

func _ready():
  _velocity = Vector2(0,0)
  _speed = Player._speed - 150
  _direction = 1

  _floor_normal = Vector2(0,-1)
  _fall_speed = 600
  _gravity = 40
  prev_pos = position.x
  $Sprite.play()

func _physics_process(delta):
  if prev_pos == position.x:
    _direction *= -1
    $Sprite.flip_h = !$Sprite.flip_h
    return
  prev_pos = position.x
  $RightWall.force_raycast_update()
  if !$RightWall.is_colliding():
    $Sprite.flip_h = !$Sprite.flip_h
    _direction *= -1
    return
  $LeftWall.force_raycast_update()
  if !$LeftWall.is_colliding():
    $Sprite.flip_h = !$Sprite.flip_h
    _direction *= -1

func _process(delta):
  pass

func _launch_attack():
  pass

func get_movement():
  _velocity.x = _direction*_speed

  # y velocity
  if _velocity.y < _fall_speed:
    _velocity.y += _gravity

  return _velocity

