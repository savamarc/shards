# This can be modified and changed to just be a base class for an ennemy
extends "res://Ennemies/Ennemy.gd"

# Time between attacks
var wait_time = 5

func _ready():
  $Timer.wait_time = wait_time
  if $Timer.connect("timeout", self, "_launch_attack"):
    print("Error")
  _velocity = Vector2(0,0)
  _speed = 175
  _direction = 1

  _floor_normal = Vector2(0,-1)
  _jump_height = -450
  _fall_speed = 600
  _gravity = 40
  _jumping_frames = 10
  _jumped_frames = 0

func _process(delta):
  pass

func _launch_attack():
  pass

func get_movement():
  # # Accelerated state.
  # # Shortcuts physics
  # if _accel:
  #   _accel_frames -= 1
  #   if _accel_frames<=0:
  #     _accel = false
  #   _velocity += _accel_value
  #   $Node2D.velocity = _velocity
  #   return _velocity

  # # Dash action
  # # Applies accelerated state and shortcuts physics
  # if _dash and Input.is_action_just_pressed("move_dash"):
  #   if !_dash.activate_ability():
  #     $Node2D.velocity = _velocity
  #     return _velocity

  # x velocity
  if Input.is_action_pressed("move_left") and Input.is_action_pressed("move_right"):
    _velocity.x = 0
  elif Input.is_action_pressed("move_right"):
    $RightWall.force_raycast_update()
    if not $RightWall.is_colliding():
      _velocity.x = _speed
      _direction = 1
      $Sprite.flip_h = false
  elif Input.is_action_pressed("move_left"):
    $LeftWall.force_raycast_update()
    if not $LeftWall.is_colliding():
      _velocity.x = -_speed
      _direction = -1
      $Sprite.flip_h = true
  else:
    _velocity.x = 0

  # y velocity
  if _velocity.y < _fall_speed:
    _velocity.y += _gravity
  if _on_floor:
    if Input.is_action_just_pressed("move_jump"):
      _velocity.y = _jump_height
      _jumped_frames = _jumping_frames
      position.y-=2
  else:
    if _jumped_frames:
      if Input.is_action_pressed("move_jump"):
        _jumped_frames -= 1
        _velocity.y = _jump_height
      elif Input.is_action_just_released("move_jump"):
        _jumped_frames = 0
      if is_on_ceiling():
        _jumped_frames = 0

  return _velocity

