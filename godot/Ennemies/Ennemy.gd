"""
This is the base class for an ennemy. No variables are initialized in this class.
It has to be expended to be used.
"""
extends KinematicBody2D

# RNG
onready var mrg64 = preload("res://bin/mrg64.gdns").new()
# The player object
var Player

# Ennemy damage for collision and hp
export (int) var dmg_value
export (int) var health_points

# Movement
var _velocity
var _speed
var _direction

## Jump related movement variables
var _floor_normal
var _jump_height
var _fall_speed
var _gravity
var _jumping_frames
var _jumped_frames
var _on_floor

func _ready():
  if $Hitbox.connect("body_entered", self, "_body_entered"):
    print("Error")
  Player = get_tree().get_root().get_node('Main/Player')

func _body_entered(body):
  body.take_dmg(dmg_value)

func _physics_process(delta):
  if !visible:
    return
  if health_points <= 0:
    return
  $FloorCheck.force_raycast_update()
  _on_floor = $FloorCheck.is_colliding()
  _velocity = move_and_slide(get_movement(), _floor_normal)

func _process(delta):
  if !visible:
    return

func take_dmg(ammount):
  health_points -= ammount
  if health_points <= 0:
    queue_free()

func get_movement():
  pass

func launch_attack():
  pass
