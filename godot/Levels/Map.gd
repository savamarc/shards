extends Node

export (Vector2) var size
export (Array) var spawn

var Limit1
var Limit2
var Limit3
var Limit4

func _ready():
  var wall = load("res://Others/Walls.tscn")
  Limit1 = wall.instance()
  Limit2 = wall.instance()
  Limit3 = wall.instance()
  Limit4 = wall.instance()

  Limit1.set_scale(Vector2(size.x/10, 1))
  Limit2.set_scale(Vector2(size.x/10, 1))
  Limit3.set_scale(Vector2(1, size.y/10))
  Limit4.set_scale(Vector2(1, size.y/10))
  Limit1.set_position(Vector2(size.x/2, -5))
  Limit2.set_position(Vector2(size.x/2, size.y+5))
  Limit3.set_position(Vector2(-5, size.y/2))
  Limit4.set_position(Vector2(size.x+5, size.y/2))
  
  Limit1.hide()
  Limit2.hide()
  Limit3.hide()
  Limit4.hide()

  add_child(Limit1)
  add_child(Limit2)
  add_child(Limit3)
  add_child(Limit4)
