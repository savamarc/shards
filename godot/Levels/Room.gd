extends Node2D

export (Vector2) var size

func _ready():
  hide()
  if $Area.connect("body_entered", self, "_area_entered"):
    print("Error")
    return
  if $Area.connect("body_exited", self, "_area_exited"):
    print("Error")
    return

func _area_entered(body):
  show()
  # Sets the camera to stay in the room
  var projectResolution = get_viewport().get_visible_rect().size

  # Shenanigans so that the camera stays centered
  if projectResolution[0] > size[0]:
    projectResolution[0] -= size[0]
    projectResolution[0] /= 2
  else:
    projectResolution[0] = 0
  if projectResolution[1] > size[1]:
    projectResolution[1] -= size[1]
    projectResolution[1] /= 2
  else:
    projectResolution[1] = 0

  var limits = [self.position.x - projectResolution[0],
    self.position.x + size[0] + projectResolution[0],
    self.position.y - projectResolution[1],
    self.position.y + size[1] + projectResolution[1]]

  # The camera is attached to the player
  body._change_camera(limits)

func _area_exited(body):
  hide()