"""
The main game logic. This contains 3 elements by default:
  - The Player node
    This contains all the information about the Player. This mostly stores the
    player's progress, but also computes the a lot of the game logic via the
    main character physics.
  - The GUI node
    This contains all the graphical elements that show informations in game
  - The GameProperties node
    This contains information about the game, such as the current map and the
    player health total. This contains information that we might need to
    interract with in multiple classes.
"""
extends Node

var Map
var PreMap

func _ready():
  # Called when the node is added to the scene for the first time.
  # Initialization here

  # A bunch of connecting stuff
  if $GameProperties.connect("player_dies", self, "_game_over"):
    print("Error")
    return
  if $GameProperties.connect("life_changed", $GUI, "update"):
    print("Error")
    return
  if $Player.connect("damage_taken", $GameProperties, "take_dmg"):
    print("Error")
    return
  if $GUI/SaveMenu.connect("save_game", self, "_save_game"):
    print("Error")
    return

  _load_game()

#func _process(delta):
#  pass

func _change_zone(new_zone, spawn_no):
  $GUI.timeout(60)
  $GUI.show_load()
  # Unloads the current map and replaces it with new_zone
  if Map:
    call_deferred('remove_child', Map)
  $GameProperties.map_area = new_zone
  var map = load($GameProperties.map_area_dict[new_zone])
  # Spawning the map
  Map = map.instance()
  add_child(Map)
  # Changing player to correct location
  $Player.call_deferred('set_position', Map.spawn[spawn_no])
  # Connecting save points
  for save in get_tree().get_nodes_in_group('SavePoints'):
    if !save.is_connected('request_save', self, '_save_prompt'):
      save.connect('request_save', self, '_save_prompt')
  #Connecting WarpZones in Map
  for node in get_tree().get_nodes_in_group("ZoneWarp"):
    if !node.is_connected("change_map_request", self, "_change_zone"):
      node.connect("change_map_request", self, "_change_zone")
  # Reseting Camera limits
  $Player/Camera.limit_top = 0
  $Player/Camera.limit_left = 0
  $Player/Camera.limit_bottom = Map.size.y
  $Player/Camera.limit_right = Map.size.x
  # Re-starting the game

func _save_prompt():
  # Pauses the game and prompts a save
  $GUI.save_prompt()

func _save_game():
  # Saves the game this is done here because we need acces to GameProperties
  var saves = get_tree().get_nodes_in_group('SavePoints')
  var save_file = File.new()
  save_file.open_encrypted_with_pass('user://data.sav', File.WRITE, '1337H4ck3r')
  for point in saves:
    if point.active:
      var data = {'Map':$GameProperties.map_area,
          'Current_life':$GameProperties.player_life,
          'Position_x':$Player.position.x,
          'Position_y':$Player.position.y}
      save_file.store_line(to_json(data))
      save_file.close()
      return
  save_file.close()

func _game_over():
  # Stops the game and gives options for the player
  $Player.hide()
  $GUI.game_over()

func _load_game():
  var save_file = File.new()
  if not save_file.file_exists('user://data.sav'):
    $GameProperties.init(0)
    _change_zone(0,0)
    return
  save_file.open_encrypted_with_pass('user://data.sav', File.READ, '1337H4ck3r')
  var data = parse_json(save_file.get_as_text())
  print(data)
  _change_zone(data['Map'], 0)
  $Player.position.x = data['Position_x']
  $Player.position.y = data['Position_y']
  $GameProperties.player_life = data['Current_life']
  save_file.close()
  var dir = Directory.new()
  if dir.open('user://') == OK:
    dir.remove('data.sav')
