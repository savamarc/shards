# Bienvenue

Ceci est le une copie en ligne d'un jeu sur lequel je travaille depuis un petit
bout. Mon but est de faire un jeu 2d (parce que j'aimerais finir un jour) de
type Metroidvania. Super Metroid est
définitivement mon influence principale, mais c'est Hollow Knight qui m'a fait
me dire qu'il fallait que je fasse quelque chose d'aussi cool. Si je vous donne
accès à ceci, c'est que j'aimerais avoir votre avis et
potentiellement votre aide pour développer le jeu. D'autres idées que les
miennes seraient les bienvenues puisque je trouve que je n'ai rien de super
original en tête. Pour l'instant, il n'y a que moi qui ai accès au projet pour
faire des modifications. Vous êtes néanmoins les bienvenus à soumettre des
suggestions, commentaires et bugs reports dans le issue tracker.

## Installer et utiliser les fichiers

Le jeu utilise le moteur open source [Godot](https://godotengine.org/). Je
développe sous Linux en essayant d'utiliser une version à jour (présentement
3.1.1). Le moteur fonctionne bien évidemment sur Windows, mais il y aura
peut-être quelques choses qui apparaîtront comme des bugs en roulant le jeu, il
faudra que je les corrige.

Pour rouler le jeu il faudra installer **Godot** (la version classique)et
télécharger les fichiers du
projet. Une fois **Godot** installé, il suffit de l'ouvrir et d'utiliser la
fonction scan sur le fichier dans lequel vous avez téléchargé le projet.
Vous devriez vous retrouver avec un projet qui s'appelle Game1. Lancez le.
Pour "tester" le jeu il suffit de faire F5.

C'est très rudimentaire. A et D pour bouger, Espace pour sauter, Clic droit pour
dash, W pour interragir avec l'environnement. Vous pouvez aussi tirer avec le
clic gauche, mais il n'y a présentement pas d'ennemis. Présentement, il n'y a
rien dans le jeu et rien à y faire, mais j'ai déjà tout ce qu'il faut pour
commencer à designer des niveaux.

### Contribuer

S'il s'avérait que vous vouliez collaborer avec moi, il faudra que vous
appreniez git un minimum pour soumettre votre travail. On s'en reparlera.

# todo list

- Calibrer la physique, demander l'avis d'autres gens pour fixer les variables
- Functional saving system
- Art, all of it
- Sons

# Les idées

Commando/mercenaire envoyé sur une planète par une fédération galactique pour aller
récupérer un objet. Au fur et à mesure de la progression, on rencontre des
locaux de moins en moins coopératifs qui faut tuer pour progresser. Au final on
découvre qu'on ne sert pas une cause particulièrement bonne...

L'idée c'est de créer un metroidvania fortement inspiré de Super Metroid. Qui
dit Metroidvania dit système d'upgrades et monde semi-ouvert qui va être
débloqué par la découverte de nouveaux pouvoirs.

## Le système d'upgrades

Je pensais peut-être à un système d'armure modulaire (comme dans Factorio au
final) avec un système de gestion des ressources sous la forme d'énergie du
genre électrique. L'idée est que les upgrades pourraient être obtenus sur les
cadavres d'anciens commandos (ou sur d'anciens commandos renégats qui ne
croyaient plus à la cause).

### Les upgrades

- Toutes les idées possibles d'armes : mêlée (épée, lance, fouet, bouclier, ...),
  distance (roquette, laser, fusil, pea-shooter, ...), bombes, grenades, tout
  type d'armes qui ressemble à de la magie comme un rayon de glace et de feu...
- Fast-travel
- Bottes statiques/générateur pour avoir de l'énergie
- Lampe/viseur infrarouge
- Mobilité classique : double/multiple-jump, wall grab/jump, jump height,
  speed-boost, dash, dive, w/e, ...
- Upgrades en puissance : armure, shield, vie (ceci dit, je pourrais faire que
  la vie ne scale pas, seulement l'armure...!), puissance de feu, réserve
  d'énergie
- Upgrades plus uniques:
  - Filtration de l'air
  - Voler?
  - Suspension boots qui permettent de marcher dans le vide
  - Véhicule? Hoverboard?
  - Nano bots (micro upgrades, heath regen)
  - Helper bots (bots volants qui assistent en ramassant des ressources ou en
    attaquant)
  - Auto défense
  - Pièces d'armure amovibles avec effet boomerang. Les utiliser réduit l'armure
    et il faut les récupérer pour récupérer l'armure. Ils peuvent être boomerang,
    mais être arrêtés par certains ennemis et tomber par terre. Ils pourraient
    être upgradés et flotter en l'air pour faire des plateformes (mettons une
    seule à la fois...)
  - Jambes/bras téléscopiques
  - Hacking/contrôle de systèmes électroniques
  - Momentum stop

Le premier upgrade est très important, parce qu'il apprend un peu le jeu au
joueur et c'est essentiellement (un des seuls) upgrades qui sera présent dans le
level design du début à la fin. Ça peut être un upgrade de mouvement, comme le
dash dans Hollow Knight. Dans ce cas, le level design pourra s'orienter plus
autour d'un mouvement fluide qui fait du jeu vraiment un jeu de plateformes.
Sinon, ça peut être un upgrade de puissance de feu. Le level design peut alors
tourner autour du fait de faire exploser des trucs. Finalement, ça peut être un
truc gimmicky, comme les bombes dans SM et la drill dans Axiom Verge. Le design
va alors tourner autour de cette gimmick.

#### Control Scheme

J'aimerais que le jeu soit jouable sur un controleur de Super Nintendo, cela
limite considérablement le nombre d'améliorations actives qu'il est possible
d'avoir. Les commandes ci-dessous sont pour un controleur de Snes (pour le
placement de a, b, x, y).

Haut   -
Bas    -
Gauche - Mouvement
Droite - Mouvement
A      -
B      - Saut
X      - Changer d'arme
Y      - Tir
L      - Tir diagonal
R      -
Start  - Pause
Select -

Combos de contrôles:
L + Haut - Tir vers le haut
L + Bas + air - Tir vers le bas

## Le monde

Prenons note d'abord que Super Metroid possède essentiellement 4 zones, mais que
chaque zone contient 2 sous zones :
  - Crateria et le Wrecked ship
  - Brinstar vert et rouge
  - Maridia dans l'eau et sans l'eau
  - Norfair upper après Kraid et lower vers Ridley
  - Tourian est une zone suplémentaire plutôt petite mais avec une ambiance
    distincte
Je pense donc qu'un jeu avec une envergure raisonnable aura autour de 10 zones
différentes.

Est-ce que je devrais faire un jeu avec une carte carrée ou plutôt plate. L'idée
est qu'une carte plus carrée est plus classique, et ça permet de mettre en place
plus de zones proches les unes des autres, mais une carte plate est une meilleure
simulation de la surface d'une planète. Sur une carte plate, il est possible
d'avoir du relief en mettant des montagnes, ou des villes "en suspension" au
dessus d'un biome.

Nota bene: si la carte est plate, les upgrades qui permettent de gagner en
mobilité verticale deviennent moins pertinents. En particulier, le premier
upgrade ne peut pas vraiment être du type double/high jump.

### L'histoire

Un(e) commando est envoyé sur une planète pour récupérer un objet spécial
(plante/pierre/animal/liquide/w/e) pour le compte d'une Fédération
interplanétaire. Cette dernière compte étudier cette objet, le cataloguer et le
stocker dans une collection.

Sur place cependant, les habitants de la planète, la faune et même la flore
s'opposent à lui/elle. C'est sans remords particuliers que cette adversité
est balayée de la surface de l'univers. À terme, après avoir cherché sur une
bonne partie de la planète, l'objet en question est enfin dans les mains du
protagoniste qui doit retourner à son vaisseau pour compléter sa mission.

Il s'avère par contre que cet objet était vital à l'écosystème de la planète.
Sur le chemin du retour, la végétation meurt derrière le joueur. Soudainement,
une monstruosité s'anime et se pose en dernier rempart
de défense de la planète. S'enclenche en même temps un décompte. Le sol
tremble pour ce combat au sommet. Après la victoire, le décompte et les
tremblement de terre continuent, c'est une course contre la montre. deux
événements différents peuvent déclencher une séquence de fin :
  - Le décompte se termine, boss vivant ou pas. C'est la fin du jeu, la
    planète explose, le personnage principal encore dessus.
  - Le personnage retourne à son vaisseau avant la fin du décompte. Alors il
    arrive à s'enfuir, la planète explosant derrière lui. Pas de "The operation
    was completed successfully par contre. Juste un petit échange de messages :
    - Package acquired. Awaiting delivery instructions.
    - Was the planet destroyed?
    - Yes. (temps de silence suivit d'un bip.) Traveling to the transmitted
      coordinates.

Le but est d'au fur et à mesure du jeu faire comprendre au joueur qu'il n'est
pas particulièrement un good guy parce que tout l'écosytème de la planète repose
sur l'objet qu'il tente de récupérer. Le but de la scène finale est alors de faire
comprendre que c'est sans remords que le personnage s'en va livrer son coli.

Peut-être ajouter des NPCS alliés et que ces npcs là se retournent contre le
personnage principal.S
